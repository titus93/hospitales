'use strict'
const Route = use('Route')
const Soap = require('soap')
const Client = require('node-rest-client').Client;
const rest = new Client();
//const dominio = 'http://localhost:3000'
const dominio = 'http://laboratoriovirtual.net'


class ReportController {
  async createPaciente({response, request, view}){

    var paciente
    try {
      paciente = await crearPaciente(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro registrar el paciente'}]

    if(paciente){
      console.log('estado', paciente.success);
      if(paciente.success){
        msg = [{'estado' : true, 'mensaje': 'Paciente registrado exitosamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': paciente.errors}]
      }
    }
    console.log(msg);
    return view.render('crearpaciente',{msg: msg})
  }

  async createCentro({response, request, view}){

    var centro
    try {
      centro = await crearCentro(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro registrar el centro de salud'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Centro registrado exitosamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearcentro',{msg: msg})
  }

  async createDoctor({response, request, view}){

    var doctor
    try {
      doctor = await crearDoctor(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro registrar el doctor'}]

    if(doctor){
      console.log('estado', doctor.success);
      if(doctor.success){
        msg = [{'estado' : true, 'mensaje': 'Doctor registrado exitosamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': doctor.errors}]
      }
    }
    console.log(msg);
    return view.render('creardoctor',{msg: msg})
  }

  async createMedicamento({response, request, view}){

    var medicamento
    try {
      medicamento = await crearMedicamento(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro registrar el medicamento'}]

    if(medicamento){
      console.log('estado', medicamento.success);
      if(medicamento.success){
        msg = [{'estado' : true, 'mensaje': 'Medicamento registrado exitosamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': medicamento.errors}]
      }
    }
    console.log(msg);
    return view.render('crearmedicamento',{msg: msg})
  }

  async createProductoMedicamento({response, request, view}){

    var producto
    try {
      producto = await crearProductoMedicamento(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro registrar el producto'}]

    if(producto){
      console.log('estado', producto.success);
      if(producto.success){
        msg = [{'estado' : true, 'mensaje': 'Producto registrado exitosamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': producto.errors}]
      }
    }
    console.log(msg);
    return view.render('crearproductomedicamento',{msg: msg})
  }

  async createConsulta({response, request, view}){

    var consulta
    try {
      consulta = await crearConsulta(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro registrar la consulta'}]

    if(consulta){
      console.log('estado', consulta.success);
      if(consulta.success){
        msg = [{'estado' : true, 'mensaje': 'Consulta registrada exitosamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': consulta.errors}]
      }
    }
    console.log(msg);
    return view.render('crearconsulta',{msg: msg})
  }

  async reportPaciente({response, request, view}){

    var datos
    try {
      datos = await getPacientes()
    } catch (e) {

    }

    return view.render('reportepaciente',{datos: datos})
  }

  async reportCentro({response, request, view}){

    var datos
    try {
      datos = await getCentros()
    } catch (e) {

    }

    return view.render('reportecentro',{datos: datos})
  }

  async reportDoctor({response, request, view}){

    var datos
    try {
      datos = await getDoctores()
    } catch (e) {

    }

    return view.render('reportedoctor',{datos: datos})
  }

  async reportMedicamento({response, request, view}){

    var datos
    try {
      datos = await getMedicamentos()
    } catch (e) {

    }

    return view.render('reportemedicamento',{datos: datos})
  }

  async reportProductoMedicamento({response, request, view}){

    var datos
    try {
      datos = await getProductoMedicamentos()
    } catch (e) {

    }

    return view.render('reporteproductomedicamento',{datos: datos})
  }

  async reportConsulta({response, request, view}){

    var datos
    try {
      datos = await getConsultas()
    } catch (e) {

    }

    return view.render('reporteconsulta',{datos: datos})
  }

  async reportReceta({response, request, view}){

    var datos
    try {
      datos = await getRecetas()
    } catch (e) {

    }

    return view.render('reportereceta',{datos: datos})
  }

  async reportRecetaMedicamento({response, request, view}){

    var datos
    try {
      datos = await getRecetaMedicamentos()
    } catch (e) {

    }

    return view.render('reporterecetamedicamento',{datos: datos})
  }

  async reportCentroId({response, request, view}){

    var datos
    try {
      datos = await getCentrosId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reportecentroid',{datos: datos})
  }

  async reportConsultaId({response, request, view}){

    var datos
    try {
      datos = await getConsultasId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reporteconsultaid',{datos: datos})
  }

  async reportDoctorId({response, request, view}){

    var datos
    try {
      datos = await getDoctoresId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reportedoctorid',{datos: datos})
  }

  async reportMedicamentoId({response, request, view}){

    var datos
    try {
      datos = await getMedicamentosId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reportemedicamentoid',{datos: datos})
  }

  async reportPacienteId({response, request, view}){

    var datos
    try {
      datos = await getPacientesId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reportepacienteid',{datos: datos})
  }

  async reportProductoMedicamentoId({response, request, view}){

    var datos
    try {
      datos = await getProductoMedicamentosId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reporteproductomedicamentoid',{datos: datos})
  }

  async reportRecetaId({response, request, view}){

    var datos
    try {
      datos = await getRecetasId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reporterecetaid',{datos: datos})
  }

  async reportRecetaMedicamentoId({response, request, view}){

    var datos
    try {
      datos = await getRecetaMedicamentosId(request.input('inId'))
    } catch (e) {

    }
    //console.log(datos);
    if(!datos.success){
      datos = null
    }else{
      var temp = []
      temp.push(datos.result)
      datos = temp
      //console.log(temp);
    }
    return view.render('reporterecetamedicamentoid',{datos: datos})
  }

  async createCentroMasivo({response, request, view}){

    var centro
    try {
      centro = await crearCentroMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearcentromasivo',{msg: msg})
  }

  async createPacienteMasivo({response, request, view}){

    var centro
    try {
      centro = await crearPacienteMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearpacientemasivo',{msg: msg})
  }

  async createDoctorMasivo({response, request, view}){

    var centro
    try {
      centro = await crearDoctorMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('creardoctormasivo',{msg: msg})
  }

  async createMedicamentoMasivo({response, request, view}){

    var centro
    try {
      centro = await crearMedicamentoMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearmedicamentomasivo',{msg: msg})
  }

  async createProductoMedicamentoMasivo({response, request, view}){

    var centro
    try {
      centro = await crearProductoMedicamentoMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearproductomedicamentomasivo',{msg: msg})
  }

  async createConsultaMasivo({response, request, view}){

    var centro
    try {
      centro = await crearConsultaMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearconsultamasivo',{msg: msg})
  }

  async createRecetaMasivo({response, request, view}){

    var centro
    try {
      centro = await crearRecetaMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearrecetamasivo',{msg: msg})
  }

  async createProductoMedicamentoMasivo({response, request, view}){

    var centro
    try {
      centro = await crearProductoMedicamentoMasivo(request)
    } catch (e) {

    }

    var msg = [{'estado' : false, 'mensaje': 'No se logro cargar el archivo'}]

    if(centro){
      console.log('estado', centro.success);
      if(centro.success){
        msg = [{'estado' : true, 'mensaje': 'Archivo cargado correctamente'}]
      }else{
        msg = [{'estado' : false, 'mensaje': centro.errors}]
      }
    }
    console.log(msg);
    return view.render('crearproductomedicamentomasivo',{msg: msg})
  }
}


  function crearPaciente (request){
    var url = dominio + '/api/pacientes/new'

    var dpi = request.input('inDPI')
    var nombres = request.input('inNombres')
    var apellidos = request.input('inApellidos')
    var telefono = request.input('inTelefono')
    var direccion = request.input('inDireccion')
    var fecha_nac = request.input('inFecha')

    var args = {
      data:{dpi:dpi, nombres:nombres, apellidos:apellidos, telefono:telefono, direccion:direccion, fecha_nac:fecha_nac},
      headers: { "Content-Type": "application/json" }
    }


    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearCentro (request){
    var url = dominio + '/api/centros/new'

    var nombre = request.input('inNombre')
    var telefono = request.input('inTelefono')
    var direccion = request.input('inDireccion')

    var args = {
      data:{nombre:nombre, telefono:telefono, direccion:direccion},
      headers: { "Content-Type": "application/json" }
    }


    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearDoctor (request){
    var url = dominio + '/api/doctores/new'

    var dpi = request.input('inDPI')
    var colegiado = request.input('inColegiado')
    var nombres = request.input('inNombres')
    var apellidos = request.input('inApellidos')
    var especialidad = request.input('inEspecialidad')
    var telefono = request.input('inTelefono')
    var direccion = request.input('inDireccion')
    var centro = request.input('inCentro')

    var args = {
      data:{dpi:dpi, colegiado:colegiado, nombres:nombres, apellidos:apellidos, especialidad:especialidad, telefono:telefono, direccion:direccion, CentroSaludId:centro},
      headers: { "Content-Type": "application/json" }
    }

    console.log(args);

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearMedicamento (request){
    var url = dominio + '/api/medicamentos/new'

    var nombre = request.input('inNombre')
    var presentacion = request.input('inPresentacion')

    var args = {
      data:{nombre:nombre, presentacion:presentacion},
      headers: { "Content-Type": "application/json" }
    }


    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearProductoMedicamento (request){
    var url = dominio + '/api/farmacia/medicamentos/new'

    var codigo = request.input('inCodigo')
    var nombre = request.input('inNombres')
    var casa = request.input('inCasa')
    var presentacion = request.input('inPresentacion')
    var stock = request.input('inStock')
    var centro = request.input('inCentro')
    var medicamento = request.input('inMedicamento')

    var args = {
      data:{codigo:codigo, nombre_comercial:nombre, casa:casa, presentacion:presentacion, stock:stock, CentroSaludId:centro, MedicamentoId:medicamento},
      headers: { "Content-Type": "application/json" }
    }

    console.log(args);
    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        //console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearConsulta (request){
    var url = dominio + '/api/consultas/new'

    var fecha = request.input('inFecha')
    var diagnostico = request.input('inDiagnostico')
    var estado = request.input('inEstado')
    var centro = request.input('inCentro')
    var doctor = request.input('inDoctor')
    var paciente = request.input('inPaciente')

    var args = {
      data:{fecha_hora:fecha, diagnostico:diagnostico, estado:estado, CentroSaludId:centro, DoctorId:doctor, PacienteId:paciente},
      headers: { "Content-Type": "application/json" }
    }
    console.log(args);
    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function getPacientes (){
    var url = dominio + '/api/pacientes/'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getCentros (){
    var url = dominio + '/api/centros/'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getDoctores (){
    var url = dominio + '/api/doctores/'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getMedicamentos (){
    var url = dominio + '/api/medicamentos/'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getProductoMedicamentos (){
    var url = dominio + '/api/farmacia/medicamentos'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getConsultas (){
    var url = dominio + '/api/consultas'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getRecetas (){
    var url = dominio + '/api/recetas'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getRecetaMedicamentos (){
    var url = dominio + '/api/detalleConsulta'

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }


  function getCentrosId (id){
    var url = dominio + '/api/centros/id/' + id
    console.log(id);
    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getPacientesId (id){
    var url = dominio + '/api/pacientes/dpi/' + id

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getDoctoresId (id){
    var url = dominio + '/api/doctores/dpi/' + id

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getMedicamentosId (id){
    var url = dominio + '/api/medicamentos/id/' + id

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getProductoMedicamentosId (id){
    var url = dominio + '/api/farmacia/medicamentos/id/' + id

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getConsultasId (id){
    var url = dominio + '/api/consultas/id/' + id

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getRecetasId (id){
    var url = dominio + '/api/recetas/id/' + id

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }

  function getRecetaMedicamentosId (id){
    var url = dominio + '/api/detalleConsulta/id/' + id

    return new Promise((resolve, reject) => {
      rest.get(url, function (data, response) {
        resolve(data)

      }).on('error', function(err){
        reject(null)
      })
    })
  }


  function crearCentroMasivo (request){
    var url = dominio + '/api/centros/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    console.log(args);


    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearPacienteMasivo (request){
    var url = dominio + '/api/pacientes/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearDoctorMasivo (request){
    var url = dominio + '/api/doctores/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearMedicamentoMasivo (request){
    var url = dominio + '/api/medicamentos/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearProductoMedicamentoMasivo (request){
    var url = dominio + '/api/farmacia/medicamentos/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearConsultaMasivo (request){
    var url = dominio + '/api/consulta/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearRecetaMasivo (request){
    var url = dominio + '/api/recetas/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }

  function crearProductoMedicamentoMasivo (request){
    var url = dominio + '/api/detalleConsulta/bulk'

    var json = request.input('textarea')

    var args = {
      data: json,
      headers: { "Content-Type": "application/json" }
    }

    return new Promise((resolve, reject) => {
      rest.post(url, args, function (data, response) {
        // parsed response body as js object

        console.log(data)
        //doctorsList = data
        resolve(data)
        //return data
      }).on('error', function(err){
        reject(err)
      })
    })
  }
module.exports = ReportController
