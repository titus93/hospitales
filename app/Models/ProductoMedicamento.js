'use strict'

const Model = use('Model')

class ProductoMedicamento extends Model {
  static get table(){
    return 'ProductoMedicamentos'
  }
}

module.exports = ProductoMedicamento
