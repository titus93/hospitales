'use strict'

const Model = use('Model')

class Receta extends Model {
  static get table(){
    return 'Receta'
  }
}

module.exports = Receta
