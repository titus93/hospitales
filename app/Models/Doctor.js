'use strict'

const Model = use('Model')

class Doctor extends Model {
  static get table(){
    return 'Doctors'
  }

}

module.exports = Doctor
