'use strict'

const Model = use('Model')

class Consulta extends Model {
  static get table(){
    return 'Consulta'
  }
}

module.exports = Consulta
