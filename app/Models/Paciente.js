'use strict'

const Model = use('Model')

class Paciente extends Model {
  static get table(){
    return 'Pacientes'
  }
}

module.exports = Paciente
