'use strict'

const Model = use('Model')

class Medicamento extends Model {
  static get table(){
    return 'Medicamentos'
  }
}

module.exports = Medicamento
