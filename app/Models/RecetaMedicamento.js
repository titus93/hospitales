'use strict'

const Model = use('Model')

class RecetaMedicamento extends Model {
  static get table(){
    return 'RecetaMedicamentos'
  }
}

module.exports = RecetaMedicamento
