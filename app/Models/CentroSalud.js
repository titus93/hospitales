'use strict'

const Model = use('Model')

class CentroSalud extends Model {
  static get table(){
    return 'CentroSaluds'
  }
}

module.exports = CentroSalud
