'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')
const Client = require('node-rest-client').Client;
const rest = new Client();
const dominio = 'http://localhost:3000'

Route.on('/').render('home')

Route.on('/crear-paciente').render('crearpaciente')
Route.post('/crear-paciente', 'ReportController.createPaciente')

Route.on('/crear-pacientemasivo').render('crearpacientemasivo')
Route.post('/crear-pacientemasivo', 'ReportController.createPacienteMasivo')

Route.get('/reporte-paciente', 'ReportController.reportPaciente')

Route.on('/reporte-pacienteid').render('reportepacienteid')
Route.post('/reporte-pacienteid', 'ReportController.reportPacienteId')



Route.on('/crear-centro').render('crearcentro')
Route.post('/crear-centro', 'ReportController.createCentro')

Route.on('/crear-centromasivo').render('crearcentromasivo')
Route.post('/crear-centromasivo', 'ReportController.createCentroMasivo')

Route.get('/reporte-centro', 'ReportController.reportCentro')

Route.on('/reporte-centroid').render('reportecentroid')
Route.post('/reporte-centroid', 'ReportController.reportCentroId')



Route.on('/crear-doctor').render('creardoctor')
Route.post('/crear-doctor', 'ReportController.createDoctor')

Route.on('/crear-doctormasivo').render('creardoctormasivo')
Route.post('/crear-doctormasivo', 'ReportController.createDoctorMasivo')

Route.get('/reporte-doctor', 'ReportController.reportDoctor')

Route.on('/reporte-doctorid').render('reportedoctorid')
Route.post('/reporte-doctorid', 'ReportController.reportDoctorId')



Route.on('/crear-medicamento').render('crearmedicamento')
Route.post('/crear-medicamento', 'ReportController.createMedicamento')

Route.on('/crear-medicamentomasivo').render('crearmedicamentomasivo')
Route.post('/crear-medicamentomasivo', 'ReportController.createMedicamentoMasivo')

Route.get('/reporte-medicamento', 'ReportController.reportMedicamento')

Route.on('/reporte-medicamentoid').render('reportemedicamentoid')
Route.post('/reporte-medicamentoid', 'ReportController.reportMedicamentoId')



Route.on('/crear-productomedicamento').render('crearproductomedicamento')
Route.post('/crear-productomedicamento', 'ReportController.createProductoMedicamento')

Route.on('/crear-productomedicamentomasivo').render('crearproductomedicamentomasivo')
Route.post('/crear-productomedicamentomasivo', 'ReportController.createProductoMedicamentoMasivo')

Route.get('/reporte-productomedicamento', 'ReportController.reportProductoMedicamento')

Route.on('/reporte-productomedicamentoid').render('reporteproductomedicamentoid')
Route.post('/reporte-productomedicamentoid', 'ReportController.reportProductoMedicamentoId')




Route.on('/crear-consulta').render('crearconsulta')
Route.post('/crear-consulta', 'ReportController.createConsulta')

Route.on('/crear-consultamasivo').render('crearconsultamasivo')
Route.post('/crear-consultamasivo', 'ReportController.createConsultaMasivo')

Route.get('/reporte-consulta', 'ReportController.reportConsulta')

Route.on('/reporte-consultaid').render('reporteconsultaid')
Route.post('/reporte-consultaid', 'ReportController.reportConsultaId')



Route.on('/crear-receta').render('crearreceta')
Route.post('/crear-receta', 'ReportController.createReceta')

Route.on('/crear-recetamasivo').render('crearrecetamasivo')
Route.post('/crear-recetamasivo', 'ReportController.createRecetaMasivo')

Route.get('/reporte-receta', 'ReportController.reportReceta')

Route.on('/reporte-recetaid').render('reporterecetaid')
Route.post('/reporte-recetaid', 'ReportController.reportRecetaId')




Route.on('/crear-recetamedicamento').render('crearrecetamedicamento')
Route.post('/crear-recetamedicamento', 'ReportController.createRecetaMedicamento')

Route.on('/crear-recetamedicamentomasivo').render('crearrecetamedicamentomasivo')
Route.post('/crear-recetamedicamentomasivo', 'ReportController.createRecetaMedicamentoMasivo')

Route.get('/reporte-recetamedicamento', 'ReportController.reportRecetaMedicamento')

Route.on('/reporte-recetamedicamentoid').render('reporterecetamedicamentoid')
Route.post('/reporte-recetamedicamentoid', 'ReportController.reportRecetaMedicamentoId')
